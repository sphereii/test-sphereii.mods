# SphereII.Mods

Test branch: A20

Notes:
  Compile using Visual Studio 2022, targeting .NET 4.8
  Designed to build and run in-place. Copy project and sln directly in the Mods folder
  
  Press Start in Debug Mode: 
    Starts the game (assuming default Steam Folder) and auto-loads the last Save game you played.
    
  Press Start in Relese Mode:
    Starts the game ( assuming default Steam folder) and loads into menu
    
    
  Tools Folder contains a unity folder, that includes a readme itself, which gives you a quick run down on how to use those files and attach a debugger.



